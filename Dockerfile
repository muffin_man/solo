FROM ubuntu:18.04 as build

RUN apt-get update && \
    apt-get install -y build-essential cmake libboost-all-dev miniupnpc \
        libunbound-dev graphviz doxygen libunwind8-dev pkg-config libssl-dev \
        libgtest-dev libreadline-dev libminiupnpc-dev \
        libzmq3-dev libpgm-dev libsodium-dev libnorm-dev git wget && \
    ln -s \
        /lib/x86_64-linux-gnu/libtinfo.so.5 /lib/x86_64-linux-gnu/libtinfo.so.6

WORKDIR /usr/src/gtest

RUN groupadd -g 1000 solo && useradd -r -u 1000 -g solo solo

RUN cmake . && make && mv libg* /usr/lib/

ARG SOLO_VERSION=1.0.4.3
RUN echo $SOLO_VERSION
ARG GIT_SOURCE=https://gitlab.com/solo-project/solo.git
ARG GIT_BRANCH=master
RUN git clone --branch $GIT_BRANCH --recursive $GIT_SOURCE /home/solo/solo;

ENV HOME /home/solo
WORKDIR /home/solo/solo
RUN git submodule init && git submodule update && mkdir -p build/release;
WORKDIR /home/solo/solo/build/release/

ARG CMAKE_OPTS=
ARG MAKE_THREADS=4
RUN cmake -D CMAKE_BUILD_TYPE=release -D STATIC=ON -D BUILD_TAG=linux-x64 \
    $CMAKE_OPTS ../.. && make -j$MAKE_THREADS

FROM ubuntu:18.04 as solo

EXPOSE 22423

RUN useradd -m solo && \
    apt-get update && \
    apt-get -qqy install libnorm1 wget && \
    apt-get -qqy clean && apt-get autoclean -qqy

USER solo
WORKDIR /home/solo/

COPY --from=build \
    /home/solo/solo/build/release/bin/ /home/solo/solo/build/release/bin

ARG BLOCK=0
RUN echo block: $BLOCK
RUN mkdir -p .solo/ && \
    wget https://minesolo.com/release/snapshot.tar.gz \
        -qO - | tar -xzf - -C .solo/

WORKDIR /home/solo/solo/build/release/bin
ENTRYPOINT [ "./solod" ]
CMD [ "--p2p-bind-ip=0.0.0.0", \
      "--rpc-bind-ip=0.0.0.0", \
      "--confirm-external-bind"]
